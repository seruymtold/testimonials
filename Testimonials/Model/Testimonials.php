<?php


namespace SGMT\Testimonials\Model;


use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\App\ObjectManager;
use SGMT\Testimonials\Api\Data\TestimonialInterface;

class Testimonials extends AbstractModel implements TestimonialInterface, IdentityInterface
{
    /**#@+
     * Testimonial's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'testimonials_testimonials';

    /**
     * @var string
     */
    protected $_cacheTag = 'testimonials_testimonials';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'testimonials_testimonials';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('SGMT\Testimonials\Model\ResourceModel\Testimonials');
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Prepare testimonial's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::TESTIMONIALS_ID);
    }
    /**
     * Get CustomerId
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return$this->getData(self::CUSTOMER_ID);
    }

    /**
     * Get Testimonials Title
     *
     * @return string
     */
    public function getTitle()
    {
        return$this->getData(self::TITLE);
    }

    /**
     * Get Testimonials Content
     *
     * @return string
     */
    public function getContent()
    {
        return$this->getData(self::CONTENT);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATED) ;
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return$this->getData(self::UPDATED);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \SGMT\Testimonials\Api\Data\TestimonialInterface
     */
    public function setId($id)
    {
        return $this->setData(self::TESTIMONIALS_ID, $id);
    }
    /**
     * Set customerID
     *
     * @param int $customerId
     * @return TestimonialInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TestimonialInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return TestimonialInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return TestimonialInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }
}