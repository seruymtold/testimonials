<?php
namespace SGMT\Testimonials\Model\ResourceModel\Testimonials;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'testimonial_id';

    protected function _construct()
    {
        $this->_init('SGMT\Testimonials\Model\Testimonials', 'SGMT\Testimonials\Model\ResourceModel\Testimonials');
    }

}