<?php

namespace SGMT\Testimonials\Model\AdminGrid\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class isActive implements OptionSourceInterface
{
    /**
     * @var \SGMT\Testimonials\Model\Testimonials
     */
    private $testimonials;

    /**
     * Constructor
     *
     * @param \SGMT\Testimonials\Model\Testimonials $testimonials
     */
    public function __construct(\SGMT\Testimonials\Model\Testimonials $testimonials)
    {
        $this->testimonials = $testimonials;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->testimonials->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
