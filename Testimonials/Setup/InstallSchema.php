<?php
namespace SGMT\Testimonials\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        /**
         * Create table 'SGMT_testimonials'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('SGMT_testimonials')
        )->addColumn(
            'testimonial_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Testimonials Id'
        )->addColumn(
            'customer_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false], 'Customer that left testimonial'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Title'
        )->addColumn(
            'content',
            Table::TYPE_TEXT,
            '1M',
            ['nullable' => false],
            'Testimonials Content'
        )->addColumn(
            'created',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addColumn(
            'is_active',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '1'],
            'Is Active'
        )->addIndex($installer->getIdxName('testimonials', ['testimonial_id']), ['testimonial_id'])
            ->addForeignKey($installer->getFkName(
                'SGMT_testimonials',
                'customer_id',
                'customer_entity',
                'entity_id'
            ),
                'customer_id', $installer->getTable('customer_entity'), 'entity_id',
                Table::ACTION_CASCADE)
            ->setComment('Testimonials');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}