<?php

namespace SGMT\Testimonials\Controller\Index;


use Magento\Framework\App\Action\Action;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use SGMT\Testimonials\Helper;
use SGMT\Testimonials\Helper\Testimonial;

class Post extends Action {

    private $customerSession;
    /**
     * @var Testimonial
     */
    private $testimonial;

    /**
     * @param Context $context
     * @param array $data
     * @internal param Session $customerSession
     */
    public function __construct(
        Context $context,
        array $data = []) {
        parent::__construct($context);
        $this->testimonial = $this->_objectManager->create('SGMT\Testimonials\Helper\Testimonial');

    }


    public function execute() {

        $post = $this->getRequest()->getParams();
        if (!$post || is_null($this->testimonial->getCustomerId())) {
            $this->messageManager->addErrorMessage(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $model = $this->_objectManager->create('SGMT\Testimonials\Model\Testimonials');
        $model->setData('customer_id', $this->testimonial->getCustomerId());
        $model->setData('title', $post['title']);
        $model->setData('content', $post['content']);

        try {
            $model->save();
            $this->_redirect('testimonials');
            $this->messageManager->addSuccessMessage(__('Your testimonial has been submitted successfully.'));

            return;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('We can\'t process your request right now. Sorry, that\'s all we know. - '.$e->getMessage())
            );
            $this->_redirect('testimonials');
            return;
        }
    }

}
