<?php

namespace SGMT\Testimonials\Controller\Adminhtml\Testimonials;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;


class MassEnable extends Action
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context)
    {

        parent::__construct($context);
    }
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $testimonialsIds = $this->getRequest()->getParam('selected');
        if (!is_array($testimonialsIds)) {
            $this->messageManager->addErrorMessage(__('Please select testimonial(s).'));
        } else {
            try {
                foreach ($testimonialsIds as $testId) {
                    $model = $this->_objectManager->create('SGMT\Testimonials\Model\Testimonials');
                    $model->load($testId);
                    $model->setIsActive(true);
                    $model->save();
                }
                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been enabled.', count($testimonialsIds))
                );
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting these records.'));
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}