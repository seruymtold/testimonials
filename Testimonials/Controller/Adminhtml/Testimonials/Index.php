<?php

namespace SGMT\Testimonials\Controller\Adminhtml\Testimonials;


use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory = false;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        //Call page factory to render layout and page content
        $resultPage = $this->resultPageFactory->create();

        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('SGMT_Testimonials::testimonials');

        //Set the header title of grid
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Testimonials'));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('SGMT'), __('SGMT'));
        $resultPage->addBreadcrumb(__('Testimonials'), __('Manage Testimonials'));

        return $resultPage;
    }

    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return true; //$this->_authorization->isAllowed('Mageplaza_Example::blog_manage');
    }
}