<?php
namespace SGMT\Testimonials\Controller\Adminhtml\Testimonials;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;

class MassDelete extends Action
{
    /**
     * @var Context
     */
    private $context;

    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }


    public function execute()
    {
        $testimonialsIds = $this->getRequest()->getParam('selected');
        if (!is_array($testimonialsIds)) {
            $this->messageManager->addErrorMessage(__('Please select testimonial(s).'));
        } else {
            try {
                foreach ($testimonialsIds as $testId) {
                    $model = $this->_objectManager->create('SGMT\Testimonials\Model\Testimonials');
                    $model->load($testId);
                    $model->delete();
                }
                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been deleted.', count($testimonialsIds))
                );
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting these records.'));
            }
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}