<?php namespace SGMT\Testimonials\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Testimonial extends AbstractHelper
{
    /**
     * @var Session $customerSession,
     */
    private $customerSession;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepositoryInterface;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepositoryInterface
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
    }

    public function getCustomerId()
    {
        return $this->customerSession->getId();
    }

    public  function getCustomerNameById($id)
    {
        $customer = $this->customerRepositoryInterface->getById($id);
        return $customer->getFirstname() . " " . $customer->getLastname();
    }

}