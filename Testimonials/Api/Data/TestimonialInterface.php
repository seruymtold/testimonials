<?php
namespace SGMT\Testimonials\Api\Data;


interface TestimonialInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const TESTIMONIALS_ID = 'testimonial_id';
    const CUSTOMER_ID   = 'customer_id';
    const TITLE = 'title';
    const CONTENT  = 'content';
    const CREATED = '	created';
    const UPDATED   = 'updated';
    const IS_ACTIVE     = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get CustomerId
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Get Testimonials Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Get Testimonials Content
     *
     * @return string
     */
    public function getContent();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();
    /**
     * Set ID
     *
     * @param int $id
     * @return \SGMT\Testimonials\Api\Data\TestimonialInterface
     */
    public function setId($id);


    /**
     * Set customerID
     *
     * @param int $customerId
     * @return \SGMT\Testimonials\Api\Data\TestimonialInterface
     */
    public function setCustomerId($customerId);

    /**
     * Set title
     *
     * @param string $title
     * @return \SGMT\Testimonials\Api\Data\TestimonialInterface
     */
    public function setTitle($title);
    /**
     * Set content
     *
     * @param string $content
     * @return \SGMT\Testimonials\Api\Data\TestimonialInterface
     */
    public function setContent($content);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \SGMT\Testimonials\Api\Data\TestimonialInterface
     */
    public function setIsActive($isActive);

}