<?php


namespace SGMT\Testimonials\Block;


use Magento\Framework\App\Request\Http;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SGMT\Testimonials\Api\Data\TestimonialInterface;
use SGMT\Testimonials\Helper\Testimonial;
use SGMT\Testimonials\Model\ResourceModel\Testimonials\CollectionFactory;

class TestimonialsList extends Template implements IdentityInterface
{
    /**
     * @var Http
     */
    private $request;
    /**
     * @var Testimonial
     */
    public $testimonial;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \SGMT\Testimonials\Model\ResourceModel\Testimonials\CollectionFactory $testimonialsCollectionFactory
     * @param Testimonial $testimonial
     * @param Http $request
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $testimonialsCollectionFactory,
        Testimonial $testimonial,
        Http $request,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_testimonialsCollectionFactory = $testimonialsCollectionFactory;

        $this->request = $request;
        $this->testimonial = $testimonial;
    }

    public function getCustomerName($id)
    {
        return $this->testimonial->getCustomerNameById($id);
    }

    /**
     * @return \SGMT\Testimonials\Model\ResourceModel\Testimonials\Collection
     */
    public function getTestimonials()
    {
        $limit = 6;
        $curr_page = 1;


        if ($this->request->get('p')) {
            $curr_page = $this->request->get('p');
        }

        //Calculate Offset
        $offset = ($curr_page - 1) * $limit;

        if (!$this->hasData('testimonials')) {
            $testimonials = $this->_testimonialsCollectionFactory
                ->create()
                ->addFilter('is_active', 1)
                ->addOrder(
                    TestimonialInterface::CREATED,
                    \SGMT\Testimonials\Model\ResourceModel\Testimonials\Collection::SORT_ORDER_DESC
                )->setPageSize($limit)->setCurPage($offset);
            $this->setData('testimonials', $testimonials);
        }
        return $this->getData('testimonials');
    }

    public function _prepareLayout()
    {
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'testimonials.pager'
        )->setCollection(
            $this->getTestimonials()
        );
        $this->setChild('pager', $pager);
        //$this->getTestimonials()->load();

        return $this;

    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [\SGMT\Testimonials\Model\Testimonials::CACHE_TAG . '_' . 'list'];
    }
}