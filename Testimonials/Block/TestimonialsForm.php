<?php

namespace SGMT\Testimonials\Block;


use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;

class TestimonialsForm extends Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */

    protected $_customerSession;

    public function __construct(Template\Context $context,
                                Session $customerSession,
                                array $data = [])
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->_customerSession = $customerSession;
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('testimonials/index/post', ['_secure' => false]);
    }

    public function isUserAllowed()
    {
        return  $this->_customerSession->isLoggedIn();

    }


}